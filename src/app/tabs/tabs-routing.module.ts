import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TabsPage } from './tabs.page';

const routes: Routes = [
  {
    path: '',
    component: TabsPage,
    children: [
      {
        path: '',
        loadChildren: () => import('../nad/nad.module').then(m => m.AtolPageModule)
      },
      {
        path: 'topping',
        loadChildren: () => import('../topping/topping.module').then(m => m.ToppingPageModule)
      },
      {
        path: 'tv',
        loadChildren: () => import('../tv/tv.module').then(m => m.TvPageModule)
      },
      {
        path: '',
        redirectTo: '/',
        pathMatch: 'full'
      }
    ]
  },
  {
    path: '',
    redirectTo: '/',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
})
export class TabsPageRoutingModule {}
