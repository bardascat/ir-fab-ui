import { IonicModule } from '@ionic/angular';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { AtolPage } from './nad.page';
import { AtolPageRoutingModule } from './nad-routing.module';

@NgModule({
  imports: [
    IonicModule,
    CommonModule,
    FormsModule,
    AtolPageRoutingModule
  ],
  declarations: [AtolPage]
})
export class AtolPageModule {}
