import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ToppingPage } from './topping.page';

const routes: Routes = [
  {
    path: '',
    component: ToppingPage,
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ToppingPageRoutingModule {}
